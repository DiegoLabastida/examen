<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<body>
<h2>Examen</h2>

<h4>Numeros primos con JS</h4>

<script>
for (var i = 2; i <= 100; i++) {
	var primo = 1;
	var contador = 2;
	while(contador <= i/2 && primo) {
		if (i % contador === 0) {
			primo = 0;
		}
		contador++;
	}
	if( primo ) {
		document.write(i + "<br />");
	}
}
</script>

<br>
<br>
<a href="/examen">Volver</a>
</body>
</html>
