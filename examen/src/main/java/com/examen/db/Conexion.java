package com.examen.db;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

public class Conexion {
	 private Connection con = null;
	 private Properties props = null;

	 public Conexion() throws IOException {
	  try {
		  props = new Properties();
		  InputStream is = ConexionBD.class.getClassLoader().getResourceAsStream("app.properties");
		  props.load(is);
		  is.close();
		  con = DriverManager.getConnection(props.getProperty("url"), props);
		  System.out.println("Conectado a la Base de Datos");
	  } catch (SQLException e) {
	   // TODO Auto-generated catch block
	   e.printStackTrace();
	  }
	 }
	 
	 public Connection getConexion(){
	  return con;
	 }
	 
	 public void cerrarConexion(){
	  try {
	   con.close();
	  } catch (SQLException e) {
	   e.printStackTrace();
	  }
	 }

}
