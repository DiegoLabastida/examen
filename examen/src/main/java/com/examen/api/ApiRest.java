package com.examen.api;

import java.io.IOException;

import javax.ws.rs.Consumes; 
import javax.ws.rs.POST;
import javax.ws.rs.Path; 
import javax.ws.rs.Produces; 
import javax.ws.rs.core.MediaType; 
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.examen.dao.FuncionesExamen;
import com.examen.modelo.ProductosTienda;

@Path("/helloworld") 
@Produces(MediaType.APPLICATION_JSON) 
@Consumes(MediaType.APPLICATION_JSON) 
public class ApiRest {
       

	    @POST
	    public ResponseBuilder adProduct(ProductosTienda productoTienda) throws IOException {  
	    	FuncionesExamen funcionesExamen = new FuncionesExamen();
	    	if( funcionesExamen.inserccion(productoTienda.getNombre()) )  {
	    		return Response.status(200);
	    	}
	    	return Response.status(500);
	    } 

	

}
