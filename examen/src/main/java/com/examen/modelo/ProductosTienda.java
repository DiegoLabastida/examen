package com.examen.modelo;

public class ProductosTienda {
	
	private int idProducto;
	private String nombre;
	private boolean estatus;
	
	public ProductosTienda() {
		super();
	}

	public ProductosTienda(int idProducto, String nombre, boolean estatus) {
		super();
		this.idProducto = idProducto;
		this.nombre = nombre;
		this.estatus = estatus;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

	@Override
	public String toString() {
		return "ProductosTienda [idProducto=" + idProducto + ", nombre=" + nombre + ", estatus=" + estatus + "]";
	}
	
	
	

}
